package com.webmages.propaganda.services;

import java.io.InputStream;
import java.lang.reflect.Method;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.webmages.propaganda.Events;

import de.greenrobot.event.EventBus;

public class TileService extends IntentService {
    public static final String TAG = TileService.class.getName();
    private Display display;
    private EventBus eventBus;
    private int screenX;
    private int screenY;
    private WallpaperManager wallpaperManager;

    public TileService() {
        super(TileService.class.getName());
    }

    @Override
    @TargetApi(17)
    public void onCreate() {
        super.onCreate();
        
        this.display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.wallpaperManager = WallpaperManager.getInstance(this);
        this.eventBus = EventBus.getDefault();

        int x = 0;
        int y = 0;

        Point point = new Point();


        if (Build.VERSION.SDK_INT < 17) {
            try {
                Method rawWidthMethod = Display.class.getMethod("getRawWidth", new Class[0]);
                rawWidthMethod.setAccessible(true);
                Method rawHeightMethod = Display.class.getMethod("getRawHeight", new Class[0]);
                rawHeightMethod.setAccessible(true);
                x = ((Integer) rawWidthMethod.invoke(this.display, new Object[0])).intValue();
                y = ((Integer) rawHeightMethod.invoke(this.display, new Object[0])).intValue();
            } catch (Exception localException) {
                while (true) {
                    Log.e(TAG, "Error getting raw width/height.", localException);
                    this.display.getSize(point);
                    int k = (int) Math.ceil(160.0F * getResources().getDisplayMetrics().density);
                    x = k + point.x;
                    y = k + point.y;
                }
            }
        }
        
        if (Build.VERSION.SDK_INT >= 17) {
            this.display.getRealSize(point);
            x = point.x;
            y = point.y;
        }

        this.screenX = (int)Math.ceil(Math.max(this.wallpaperManager.getDesiredMinimumWidth(), 2.5D * Math.min(x, y)));
        this.screenY = (int)Math.ceil(Math.max(this.wallpaperManager.getDesiredMinimumHeight(), Math.max(x, y)));

    }

    @Override
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    protected void onHandleIntent(Intent intent) {
        InputStream is = null;
        Bitmap source = null;
        Bitmap backing = null;
        //String volumeName = intent.getStringExtra("volumeId");
        String drawableName = intent.getStringExtra("drawableName");
        try {
        
        	source = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(drawableName, "drawable", getPackageName()));
            
            source.setHasAlpha(false);
        
            // load the bitmap
            BitmapDrawable tile = new BitmapDrawable(getResources(), source);
            tile.setTileModeX(Shader.TileMode.REPEAT);
            tile.setTileModeY(Shader.TileMode.REPEAT);
            tile.setBounds(0,0,screenX,screenY);
            
            backing = Bitmap.createBitmap(screenX,screenY,Bitmap.Config.ARGB_8888);
            backing.setDensity(Bitmap.DENSITY_NONE);
            
            backing.setHasAlpha(false);
            Canvas canvas = new Canvas(backing);
            canvas.setDensity(Bitmap.DENSITY_NONE);
            tile.draw(canvas);
            
            wallpaperManager.forgetLoadedWallpaper();
            wallpaperManager.setBitmap(backing);
            eventBus.post(new Events.BackgroundSetEvent(drawableName));
        
        }
        catch (Exception e) {
            Log.e(TAG,"There was a problem loading the tile.",e);
            eventBus.post(new Events.BackgroundFailedEvent(drawableName));
        }
        finally {
            if (is != null) {
                try { is.close(); } catch (Exception e) { Log.e(TAG,"Error closing input stream.",e);
                }
            }
            if (source != null) {
                source.recycle();
                source = null;
            }
            if (backing != null) {
                backing.recycle();
                backing = null;
            }
        }

   }
}