package com.webmages.propaganda;

public class Events {
    public static class BackgroundFailedEvent {
        public final String mDrawableName;

        public BackgroundFailedEvent(String drawableName) {
            this.mDrawableName = drawableName;
        }

        public String getDrawableName() {
            return this.mDrawableName;
        }
    }

    public static class BackgroundSetEvent {
        public final String mDrawableName;

        public BackgroundSetEvent(String drawableName) {
            this.mDrawableName = drawableName;
        }

        public String getDrawableName() {
            return this.mDrawableName;
        }
    }
}