package com.webmages.propaganda.activities;

import org.apache.commons.lang3.text.WordUtils;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.webmages.propaganda.Events;
import com.webmages.propaganda.PropagandaApplication;
import com.webmages.propaganda.R;

import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class BrowserActivity extends FragmentActivity implements VolumePositioner {
    public static final String TAG = BrowserActivity.class.getName();
    private PropagandaApplication application;
    private int volumePosition;

    @Override
    public int getVolumePosition() {
        return this.volumePosition;
    }

    @Override
    public void onResume() {
    	super.onResume();
    	EventBus.getDefault().register(this,Events.BackgroundSetEvent.class,Events.BackgroundFailedEvent.class);
    }
    
    @Override
    public void onPause() {
    	EventBus.getDefault().unregister(this,Events.BackgroundSetEvent.class,Events.BackgroundFailedEvent.class);    	
    	super.onPause();
    }
    
    public void onEventMainThread(Events.BackgroundSetEvent event) {
    	Crouton.makeText(this, WordUtils.capitalizeFully(event.getDrawableName().replaceAll("_", " ")) + " is now your background.", Style.INFO).show();
    }

    public void onEventMainThread(Events.BackgroundFailedEvent event) {
    	Crouton.makeText(this, "Failed to set " + WordUtils.capitalizeFully(event.getDrawableName().replaceAll("_", " ")) + " as your background.", Style.ALERT).show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int i = getIntent().getIntExtra("position", -1);
        if (i > -1)
            setVolumePosition(i);
        setContentView(R.layout.browser_load);
        ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        this.application = ((PropagandaApplication) getApplication());
        this.volumePosition = getIntent().getIntExtra("position", 0);
        

        actionBar.setTitle(this.application.getVolumeSubTitles()[this.volumePosition]);
        
        if (actionBar.getTitle() != null && !actionBar.getTitle().toString().trim().equals(""))
        	actionBar.setSubtitle(this.application.getVolumeNames()[this.volumePosition]);
        else
        	actionBar.setTitle(this.application.getVolumeNames()[this.volumePosition]);
    }

    @Override
    public void setVolumePosition(int paramInt) {
        this.volumePosition = paramInt;
        Fragment localFragment = getSupportFragmentManager().findFragmentById(R.id.browserfragment);
        if ((localFragment != null) && ((localFragment instanceof VolumePositioner)))
            ((VolumePositioner) localFragment).setVolumePosition(paramInt);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_about:
            	Intent i = new Intent(this,AboutActivity.class);
            	startActivity(i);
            	break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.about, menu);
        return true;
    }
}