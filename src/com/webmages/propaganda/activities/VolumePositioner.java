package com.webmages.propaganda.activities;

public abstract interface VolumePositioner {
    public abstract int getVolumePosition();
    public abstract void setVolumePosition(int paramInt);
}