package com.webmages.propaganda;

import uk.co.senab.bitmapcache.BitmapLruCache;
import android.app.Application;

public class PropagandaApplication extends Application {
    public BitmapLruCache bitmapCache;
    private String[] volumeIds;
    private String[] volumeNames;
    private String[] volumeSubTitles;

    public String[] getImages(int paramInt) {
        return getResources().getStringArray(getResources().getIdentifier(this.volumeIds[paramInt], "array", getPackageName()));
    }

    public String[] getVolumeIds() {
        return this.volumeIds;
    }

    public String[] getVolumeNames() {
        return this.volumeNames;
    }

    public String[] getVolumeSubTitles() {
        return this.volumeSubTitles;
    }

    @Override
    public void onCreate() {
        this.volumeIds = getResources().getStringArray(R.array.volumeIds);
        this.volumeNames = getResources().getStringArray(R.array.volumeNames);
        this.volumeSubTitles = getResources().getStringArray(R.array.volumeSubTitles);
        this.bitmapCache = new BitmapLruCache.Builder(this).setDiskCacheEnabled(false).setMemoryCacheEnabled(true).setMemoryCacheMaxSizeUsingHeapSize(0.3F).build();
    }
}