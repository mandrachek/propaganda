package com.webmages.propaganda.adapters;

import com.webmages.propaganda.fragments.AboutFragment;
import com.webmages.propaganda.fragments.CreditsFragment;
import com.webmages.propaganda.fragments.LicenseFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class AboutAdapter extends FragmentPagerAdapter {
	public static final String TAG = AboutAdapter.class.getName();
	
	public AboutAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		Log.w(TAG,"arg: " + position);
		switch (position) {
			case 0: return new AboutFragment();
			case 1: return new LicenseFragment();
			case 2: return new CreditsFragment();
			default: return null;
		}

	}
	
	@Override
    public CharSequence getPageTitle(int position) {
		switch (position) {
			case 0: return "PROPAGANDA";
			case 1: return "LICENSE";
			case 2: return "CREDITS";
			default: return null;
		}
		
	}


	@Override
	public int getCount() {
		return 3;
	}

}
