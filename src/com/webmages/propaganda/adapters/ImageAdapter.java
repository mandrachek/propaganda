package com.webmages.propaganda.adapters;

import java.lang.ref.WeakReference;

import org.apache.commons.lang3.text.WordUtils;

import uk.co.senab.bitmapcache.BitmapLruCache;
import uk.co.senab.bitmapcache.CacheableBitmapDrawable;
import uk.co.senab.bitmapcache.CacheableImageView;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.webmages.propaganda.PropagandaApplication;
import com.webmages.propaganda.R;
import com.webmages.propaganda.activities.VolumePositioner;
import com.webmages.propaganda.fragments.ScrollPauser;


public class ImageAdapter extends BaseAdapter {
    public static final String TAG = ImageAdapter.class.getName();
    private PropagandaApplication application;
    private BitmapLruCache bitmapCache;
    private Context mContext;
    private final String[] mImages;
    private ScrollPauser mPauser;
    private VolumePositioner mPositioner;
    private final String mVolumeId;

    public ImageAdapter(Context paramContext, ScrollPauser paramScrollPauser,
            VolumePositioner paramVolumePositioner) {
        this.mContext = paramContext;
        this.application = ((PropagandaApplication) this.mContext.getApplicationContext());
        this.mPositioner = paramVolumePositioner;
        this.mPauser = paramScrollPauser;
        this.mVolumeId = this.application.getVolumeIds()[this.mPositioner.getVolumePosition()];
        this.mImages = this.application.getImages(this.mPositioner.getVolumePosition());
        this.bitmapCache = this.application.bitmapCache;
    }

    @Override
    public int getCount() {
        return this.mImages.length;
    }

    @Override
    public Object getItem(int paramInt) {
        return null;
    }

    @Override
    public long getItemId(int paramInt) {
        return 0L;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.tile, parent, false);
        }

        CacheableImageView imageView = (CacheableImageView) view.findViewById(R.id.tile);
        ((TextView) view.findViewById(R.id.title)).setText(WordUtils.capitalizeFully(this.mImages[position].replace("_", " ")));
        String str = this.mImages[position];
        
        // image is already set, no need to reset it.
        if (imageView.getTag() != null && ((String)imageView.getTag()).equals(str) && imageView.getDrawable() != null) {
        	return view;
        }
        
        
        // new image required.
        imageView.setTag(str);

        // check to see if we have it in the bitmap cache.
        if (this.bitmapCache.containsInMemoryCache(str)) {
            imageView.setImageDrawable(this.bitmapCache.getFromMemoryCache(str));
        } else {
        	// no? empty it out.
        	imageView.setImageDrawable(null);
        	// only attempt to load if we're "paused" - i.e., not scrolling.
        	if (!this.mPauser.isPaused()) {
        		DiskLoaderTask localDiskLoaderTask = new DiskLoaderTask(this.mContext, imageView, this.mVolumeId, this.mImages[position]);
        		localDiskLoaderTask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, new Void[0]);
        	}
        }

        return view;
    }

    private class DiskLoaderTask extends AsyncTask<Void, Void, Bitmap> {
        private final WeakReference<Context> mContext;
        private final String mDrawableName;
        private final WeakReference<CacheableImageView> mImageView;
        private final String mTag;

        public DiskLoaderTask(Context context, CacheableImageView imageView, String volumeId, String drawableName) {
            this.mContext = new WeakReference<Context>(context);
            this.mImageView = new WeakReference<CacheableImageView>(imageView);
            this.mDrawableName = drawableName;
            
            this.mTag = drawableName;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
        	Bitmap bitmap = null;
            try {
            	Context context = mContext.get();
            	if (context == null) return null;
            	bitmap = 
            			BitmapFactory.decodeResource(context.getResources(),context.getResources().getIdentifier(this.mDrawableName, "drawable", context.getPackageName()));
            } catch (Exception e) {
                    Log.e(ImageAdapter.TAG, "An error occurred decoding webp file", e);
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
        	if (bitmap == null) {
        		return;
        	}
        	CacheableBitmapDrawable drawable = ImageAdapter.this.bitmapCache.put(mDrawableName, bitmap);
            CacheableImageView localCacheableImageView = (CacheableImageView) this.mImageView.get();
            if ((localCacheableImageView != null) && (((String) localCacheableImageView.getTag()).equals(this.mTag))) {
                if (this.mContext.get() != null && bitmap != null)
                    localCacheableImageView.startAnimation(AnimationUtils.loadAnimation((Context) this.mContext.get(), R.anim.fadein));
                	localCacheableImageView.setImageDrawable(drawable);
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... paramArrayOfVoid) {
        }
    }
}