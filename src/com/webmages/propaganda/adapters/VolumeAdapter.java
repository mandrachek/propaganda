package com.webmages.propaganda.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.webmages.propaganda.PropagandaApplication;
import com.webmages.propaganda.R;

public class VolumeAdapter extends ArrayAdapter<String> {
    public static final String TAG = VolumeAdapter.class.getName();
    private final PropagandaApplication application;
    private final Context mContext;

    public VolumeAdapter(Context context) {
        super(context, R.layout.volumerow, context.getResources().getStringArray(R.array.volumeIds));
        this.mContext = context;
        this.application = ((PropagandaApplication) context.getApplicationContext());
    }

    @Override
    public View getView(int position, View contextView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = contextView;
        VolumeHolder volumeHolder = null;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.volumerow, parent, false);
            volumeHolder = new VolumeHolder();
            volumeHolder.volumeName = ((TextView) view.findViewById(R.id.volumename));
            volumeHolder.volumeTitle = ((TextView) view.findViewById(R.id.volumetitle));
            view.setTag(volumeHolder);
        } else {
            volumeHolder = (VolumeHolder) view.getTag();
        }
        
        volumeHolder.volumeName.setText(this.application.getVolumeNames()[position]);
        volumeHolder.volumeTitle.setText(this.application.getVolumeSubTitles()[position]);
        
        if ((position+1) <= this.application.getVolumeSubTitles().length) {
        	if (this.application.getVolumeSubTitles()[position].trim().equals("")) {
            	volumeHolder.volumeName.setVisibility(View.GONE);
                volumeHolder.volumeTitle.setText(this.application.getVolumeNames()[position]);	
        	}
        	else {
            	volumeHolder.volumeName.setVisibility(View.VISIBLE);
        		
        	}
        	
        }
        
        return view;
    }

    static class VolumeHolder {
        public TextView volumeName;
        public TextView volumeTitle;
    }
}