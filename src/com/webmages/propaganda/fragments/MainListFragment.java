package com.webmages.propaganda.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.webmages.propaganda.R;
import com.webmages.propaganda.activities.BrowserActivity;
import com.webmages.propaganda.adapters.VolumeAdapter;

public class MainListFragment extends ListFragment implements AdapterView.OnItemClickListener {
    @Override
    public void onActivityCreated(Bundle paramBundle) {
        super.onActivityCreated(paramBundle);
        setListAdapter(new VolumeAdapter(getActivity()));
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup paramViewGroup, Bundle paramBundle) {
        return layoutInflater.inflate(R.layout.main_list, paramViewGroup, true);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent localIntent = new Intent(getActivity(), BrowserActivity.class);
        localIntent.putExtra("position", position);
        startActivity(localIntent);
    }

    @Override
    public void onViewCreated(View paramView, Bundle paramBundle) {
        super.onViewCreated(paramView, paramBundle);
        getListView().setOnItemClickListener(this);
    }

}