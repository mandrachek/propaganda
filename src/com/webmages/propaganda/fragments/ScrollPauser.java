package com.webmages.propaganda.fragments;

public abstract interface ScrollPauser
{
  public abstract boolean isPaused();
}