package com.webmages.propaganda.fragments;

import org.apache.commons.lang3.text.WordUtils;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import com.webmages.propaganda.PropagandaApplication;
import com.webmages.propaganda.R;
import com.webmages.propaganda.activities.VolumePositioner;
import com.webmages.propaganda.adapters.ImageAdapter;
import com.webmages.propaganda.services.TileService;

public class BrowserFragment extends Fragment implements AdapterView.OnItemClickListener, AbsListView.OnScrollListener, VolumePositioner, ScrollPauser {
    public static final String TAG = BrowserFragment.class.getName();
    private PropagandaApplication application;
    private String[] images;
    private GridView mGridView;
    private int mVolumePosition = -1;
    private volatile boolean paused = false;

    @Override
    public int getVolumePosition() {
        return this.mVolumePosition;
    }

    @Override
    public boolean isPaused() {
        return this.paused;
    }

    @Override
    public void onActivityCreated(Bundle paramBundle) {
        super.onActivityCreated(paramBundle);
        if ((getActivity() instanceof VolumePositioner))
            setVolumePosition(((VolumePositioner) getActivity()).getVolumePosition());
        if (getVolumePosition() > -1) {
            this.images = getResources().getStringArray(getResources().getIdentifier(this.application.getVolumeIds()[getVolumePosition()], "array", getActivity().getPackageName()));
            this.mGridView.setAdapter(new ImageAdapter(getActivity(), this, this));
        }
    }

    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        this.application = ((PropagandaApplication) getActivity().getApplication());
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup paramViewGroup, Bundle paramBundle) {
        View localView = layoutInflater.inflate(R.layout.browser, paramViewGroup, true);
        this.mGridView = ((GridView) localView.findViewById(R.id.gridview));
        this.mGridView.setOnItemClickListener(this);
        this.mGridView.setOnScrollListener(this);
        return localView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        SetWallpaperDialogFragment.newInstance(this.mVolumePosition, this.images, position).show(getFragmentManager(), "setwallpaper");
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibileItem, int visibleItemCount, int totalItemCount) {
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
            this.paused = false;
            view.invalidateViews();
        } else {
            this.paused = true;
        }
    }

    @Override
    public void setVolumePosition(int paramInt) {
        this.mVolumePosition = paramInt;
    }

    public static class SetWallpaperDialogFragment extends
            DialogFragment {
        public static SetWallpaperDialogFragment newInstance(int paramInt1, String[] paramArrayOfString, int paramInt2) {
            SetWallpaperDialogFragment localSetWallpaperDialogFragment = new SetWallpaperDialogFragment();
            Bundle localBundle = new Bundle();
            localBundle.putInt("volumePosition", paramInt1);
            localBundle.putInt("imagePosition", paramInt2);
            localBundle.putStringArray("images", paramArrayOfString);
            localSetWallpaperDialogFragment.setArguments(localBundle);
            return localSetWallpaperDialogFragment;
        }

        private void setWallpaper(String paramString1, String paramString2) {
            Intent localIntent = new Intent(getActivity(), TileService.class);
            localIntent.putExtra("drawableName", paramString2);
            localIntent.putExtra("volumeId", paramString1);
            getActivity().startService(localIntent);
        }

        @Override
        public void onCreate(Bundle paramBundle) {
            super.onCreate(paramBundle);
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
        public Dialog onCreateDialog(Bundle paramBundle) {
            final int volumePosition = getArguments().getInt("volumePosition");
            final String[] images = getArguments().getStringArray("images");
            final int imagePosition = getArguments().getInt("imagePosition");
            AlertDialog.Builder alertBuilder = null;
            if (Build.VERSION.SDK_INT >= 11)
            	alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Dialog));
            else
            	alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Dialog));
            	
            alertBuilder.setCancelable(true).setIcon(R.drawable.ic_launcher).setTitle("JFK Wants To Know:").setMessage("Would you like to use " + WordUtils.capitalizeFully(images[imagePosition].replaceAll("_", " ")) + " as your wallpaper?").setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SetWallpaperDialogFragment.this.setWallpaper(((PropagandaApplication)getActivity().getApplication()).getVolumeIds()[volumePosition],images[imagePosition]);
                    dialog.dismiss();
                }
            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            return alertBuilder.create();
        }
        
    }

}